#include "Arduino.h"
#include "eeprom.h"

namespace controller {

extern bool heaterState;
extern String workMode;
extern int powerLimit;
extern int hysteresis;


void setup();
String getHeaterState();
void heaterTriggering(float power);
void disableHeater();
void enableHeater();
}