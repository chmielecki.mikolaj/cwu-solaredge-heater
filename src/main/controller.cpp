#include "Arduino.h"
#include "controller.h"
#include "constants.h"

namespace controller {

bool heaterState = false;
String workMode = "OFF";
int powerLimit = 3000;
int hysteresis = 100;


void setup() {

  pinMode(constants::CONTROLLER_OUTPUT, OUTPUT);
  delay(50);
  digitalWrite(constants::CONTROLLER_OUTPUT, LOW);


  Serial.println("Reading EEPROM...");
  workMode = eeprom::getWorkMode();
  powerLimit = eeprom::getPowerLimit();
  hysteresis = eeprom::getHysteresis();
  Serial.print("Work mode: ");
  Serial.println(workMode);
  Serial.print("Limit power: ");
  Serial.println(powerLimit);
  Serial.print("Hysteresis: ");
  Serial.println(hysteresis);
}

String getHeaterState() {
  if (heaterState) {
    return "ON";
  }
  return "OFF";
}

void heaterTriggering(float power) {

  if (workMode == "ON") {
    enableHeater();
    return;
  }

  if (workMode == "OFF") {
    disableHeater();
    return;
  }

  if (workMode == "AUTO") {
    if (power < powerLimit - hysteresis && heaterState) {
      disableHeater();
    }
    if (power > powerLimit + hysteresis && !heaterState) {
      enableHeater();
    }
    return;
  }

  disableHeater();
}

void enableHeater() {
  heaterState = true;
  Serial.println("Heater enabled");
  digitalWrite(constants::CONTROLLER_OUTPUT, HIGH);
}

void disableHeater() {
  heaterState = false;
  Serial.println("Heater disabled");
  digitalWrite(constants::CONTROLLER_OUTPUT, LOW);
}

}