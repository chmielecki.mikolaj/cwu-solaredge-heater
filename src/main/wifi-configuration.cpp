#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include "wifi-configuration.h"
#include "constants.h"
#include "wifi-configuration-html.h"
#include "eeprom.h"
#include "json.h"

namespace wifi_configuration {

ESP8266WebServer server(80);
int _i = 0;
int _statusCode;
const char *_ssid = "text";
const char *_passphrase = "text";
String _wifiSSIDs;
String _content;

void setup() {

  Serial.println();
  Serial.println("Disconnecting previously connected WiFi");
  WiFi.disconnect();

  delay(constants::SMALL_DELAY);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.println("Startup");
  Serial.println("Reading EEPROM");

  String essid = eeprom::getSsid();
  Serial.print("SSID: ");
  Serial.println(essid);

  String epass = eeprom::getPass();
  Serial.print("Password: ");
  Serial.println(epass);

  String eip = eeprom::getIp();
  Serial.print("IP: ");
  Serial.println(eip);

  String egateway = eeprom::getGatewayIp();
  Serial.print("Gateway ip: ");
  Serial.println(egateway);



  // Set your Static IP address
  IPAddress localIp;
  localIp.fromString(eip);
  // Set your Gateway IP address
  IPAddress gateway;
  gateway.fromString(egateway);

  IPAddress subnet(255, 255, 255, 0);
  IPAddress primaryDNS(8, 8, 8, 8);    //optional
  IPAddress secondaryDNS(8, 8, 4, 4);  //optional
  // Configures static IP address
  if (!WiFi.config(localIp, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }
  WiFi.begin(essid.c_str(), epass.c_str());
  if (_testWifi()) {
    Serial.println();
    Serial.println("Succesfully Connected!!!");
    Serial.print("IP: ");
    Serial.println(WiFi.localIP());
    server.stop();
    return;
  }

  setupWifiConfiguration();
}

void setupWifiConfiguration() {
  Serial.println("Turning the HotSpot On");
  _launchWifiConfig();
  _setupAP();  // Setup HotSpot


  Serial.println();
  Serial.println("Waiting.");

  while (true) {
    Serial.print(".");
    delay(constants::NORMAL_DELAY);
    server.handleClient();
  }
}


//-------- Fuctions used for WiFi credentials saving and connecting to it which you do not need to change
bool _testWifi(void) {
  int c = 0;
  Serial.println("Waiting for Wifi to connect");
  while (c < 100) {
    if (WiFi.status() == WL_CONNECTED) {
      return true;
    }
    delay(constants::LARGE_DELAY);

    if (c % 2) {
      digitalWrite(LED_BUILTIN, LOW);
    } else {
      digitalWrite(LED_BUILTIN, HIGH);
    }

    Serial.print("*");
    c++;
  }
  Serial.println("");
  Serial.println("Connect timed out, opening AP");
  return false;
}

void _launchWifiConfig() {
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  _configureEndpoints();

  // Start the server
  server.begin();
  Serial.println("Server started");
}

void _setupAP(void) {
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(constants::NORMAL_DELAY);
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0)
    Serial.println("no networks found");
  else {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i) {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.println(")");
      delay(constants::SMALL_DELAY);
    }
  }
  Serial.println("");
  _wifiSSIDs = "";
  for (int i = 0; i < n; ++i) {
    _wifiSSIDs += "<option>";
    _wifiSSIDs += WiFi.SSID(i);
    _wifiSSIDs += "</option>";
  }
  delay(constants::NORMAL_DELAY);
  WiFi.softAP("cwu-solaredge", "");
  Serial.println("Starting softap");
  _launchWifiConfig();
  Serial.println("Starting softap over");
}

void _configureEndpoints() {
  {
    server.on("/", []() {
      _content = wifi_configuration::getHtml(_wifiSSIDs);
      server.send(200, "text/html", _content);
      delay(constants::SERVER_RESPONSE_DELAY);
    });

    server.on("/settings", []() {
      String qssid = server.arg("ssid");
      String qpass = server.arg("pass");
      String qip = server.arg("ip");
      String qgateway = server.arg("gateway");
      if (qssid.length() > 0 && qpass.length() > 0 && qip.length() > 0 && qgateway.length() > 0) {

        Serial.print("writing eeprom ssid:");
        Serial.println(qssid);
        eeprom::setSsid(qssid);

        Serial.print("writing eeprom pass:");
        Serial.println(qpass);
        eeprom::setPass(qpass);

        Serial.print("writing eeprom ip:");
        Serial.println(qip);
        eeprom::setIp(qip);

        Serial.print("writing eeprom gateway:");
        Serial.println(qgateway);
        eeprom::setGatewayIp(qgateway);

        eeprom::commit();

        server.send(200, "application/json", json::message("saved to eeprom... reset to boot into new wifi"));
        delay(constants::SERVER_RESPONSE_DELAY);

        ESP.reset();
        return;
      }
    });
  }
}

}