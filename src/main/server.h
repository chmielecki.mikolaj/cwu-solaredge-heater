namespace server {

void setup();
void loop();

void _configureEndpoints();
bool _enableLed(void *enabled);
void _configTimers();
void _rootController();
void _inverterController();
void _settingsController();
String _prepareWorkModes();
}