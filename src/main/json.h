#include "Arduino.h"

namespace json {
  String message(String message);
  String prepareJson(String* keys, String* values, int n);
}