#include "json.h"

namespace json {
  String message(String message) {
    String keys[] = {"message"};
    String values[] = {message};
    return prepareJson(keys, values, 1);
  }

  String prepareJson(String* keys, String* values, int n) {
    String result = "{";

    for(int i = 0; i < n; i++) {
      result += "\"" + keys[i] + "\":\"" + values[i] + "\"";
      if (i != n - 1) {
        result += ",";
      }
    }

    result += "}";
    return result;
  }

}