#include <EEPROM.h>
#include "wifi-configuration.h"
#include "server.h"
#include "controller.h"
#include "inverter.h"
#include "inverter-connection.h"
#include "restart-schedule.h"

void setup() {
  Serial.begin(9600);
  EEPROM.begin(1024);

  wifi_configuration::setup();
  server::setup();
  controller::setup();
  inverter::setup();
  inverterConnection::setup();
  restart_schedule::setup();
  
}

void loop() {
  server::loop();
  inverter::loop();
  restart_schedule::loop();
}