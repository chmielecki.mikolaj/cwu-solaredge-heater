#include "Arduino.h"
#include "eeprom.h"
#include <ModbusIP_ESP8266.h>



namespace inverter {
  extern String inverterIp;
  extern int inverterPort;
  extern ModbusIP mb;
  extern IPAddress remote;
  extern bool isConnectedToInverter;


  void setup();
  void loop();
  float getPower();
  void getVoltage(float* result);
  float getFrequency();
  void setInverterIp(String ip);
  bool _inverterRefreshing(void *);
}