#include "Arduino.h"
#include "restart-schedule.h"
#include <NTPClient.h>
#include "constants.h"
#include <arduino-timer.h>
#include <WiFiUdp.h>

namespace restart_schedule {

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 3600 * 2);
auto _ntpTimer = timer_create_default();


void setup() {
  timeClient.begin();
  _ntpTimer.every(constants::CHECK_NTP_TIME, _scheduledTask);
}
void loop() {
  _ntpTimer.tick();
}

bool _scheduledTask(void *) {
  timeClient.update();

  if (timeClient.getHours() == 3 && millis() > constants::MIN_WORKING_TIME) {
    Serial.println("Schedule restarting...");
    ESP.reset();
  }

  return true;
}
}
