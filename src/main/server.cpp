#include <arduino-timer.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>


#include "server.h"
#include "constants.h"
#include "wifi-configuration.h"
#include "json.h"
#include "eeprom.h"
#include "settings-html.h"
#include "dashboard-html.h"
#include "inverter.h"
#include "controller.h"

namespace server {

ESP8266WebServer server(80);
auto _statusTimer = timer_create_default();
auto _serverTimer = timer_create_default();

void setup() {
  digitalWrite(LED_BUILTIN, HIGH);

  _configureEndpoints();
  server.begin();

  _configTimers();
}

void loop() {
  _statusTimer.tick();
  _serverTimer.tick();
}

void _configureEndpoints() {
  _rootController();
  _settingsController();
  _inverterController();

  server.on("/restart", HTTP_GET, []() {
    Serial.println("GET: /restart");
    server.send(200, "application/json", json::message("restarting..."));
    delay(constants::SERVER_RESPONSE_DELAY);
    server.stop();
    wifi_configuration::setupWifiConfiguration();
  });
}

bool _handleClientRequest(void *) {
  server.handleClient();
  return true;
}

bool _enableLed(void *enabled) {

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("WiFi disonnected. Restarting...");
    ESP.reset();
  }

  bool value = enabled;
  if (value) {
    digitalWrite(LED_BUILTIN, LOW);
    _statusTimer.in(constants::LED_ON_TIME, _enableLed, (void *)false);
  } else {
    digitalWrite(LED_BUILTIN, HIGH);

    if (inverter::isConnectedToInverter) {
      _statusTimer.in(constants::LED_OFF_TIME, _enableLed, (void *)true);
    } else {
      _statusTimer.in(constants::LED_OFF_TIME_INVERTER_CONNECTING, _enableLed, (void *)true);
    }
  }
  return false;
}

void _configTimers() {
  _statusTimer.in(constants::LED_OFF_TIME, _enableLed, (void *)true);
  _serverTimer.every(constants::NORMAL_DELAY, _handleClientRequest);
}

void _rootController() {
  server.on("/", HTTP_GET, []() {
    Serial.println("GET: /");

    if (!inverter::isConnectedToInverter) {
      server.send(200, "text/html", dashboard::getHtml("-", "-", "-", "-", "-", String(controller::powerLimit), String(controller::hysteresis), controller::workMode, controller::getHeaterState()));
      return;
    }

    const String inverterPower = String(inverter::getPower());
    float voltage[3] = { -1, -1, -1 };
    inverter::getVoltage(voltage);
    float frequency = inverter::getFrequency();
    server.send(200, "text/html", dashboard::getHtml(inverterPower, String(voltage[0]), String(voltage[1]), String(voltage[2]), String(frequency), String(controller::powerLimit), String(controller::hysteresis), controller::workMode, controller::getHeaterState()));
  });
}

void _inverterController() {
  server.on("/inverter", HTTP_GET, []() {
    Serial.println("GET: /inverter");

    if (!inverter::isConnectedToInverter) {
      String keys[] = { "message", "heaterState"};
      String values[] = { "Nie połączono z falownikiem", controller::getHeaterState()};
      server.send(400, "application/json", json::prepareJson(keys, values, 2));
      return;
    }

    const String inverterState = String(inverter::getPower());
    float voltage[3] = { -1, -1, -1 };
    inverter::getVoltage(voltage);
    float frequency = inverter::getFrequency();
    String keys[] = { "inverterPower", "heaterState", "f", "l1", "l2", "l3" };
    String values[] = { inverterState, controller::getHeaterState(), String(frequency), String(voltage[0]), String(voltage[1]), String(voltage[2]) };
    server.send(200, "application/json", json::prepareJson(keys, values, 6));
  });
}

void _settingsController() {
  server.on("/settings", HTTP_GET, []() {
    Serial.println("GET: /settings");
    server.send(200, "text/html", settings::getHtml(inverter::inverterIp, String(inverter::inverterPort), String(controller::powerLimit), String(controller::hysteresis), _prepareWorkModes()));
  });

  server.on("/settings", HTTP_POST, []() {
    Serial.println("POST: /settings");
    String newInverterIp = server.arg("inverterIp");
    String newInverterPort = server.arg("inverterPort");
    String newPowerLimit = server.arg("powerLimit");
    String newHysteresis = server.arg("hysteresis");
    String newWorkMode = server.arg("workMode");

    Serial.println("Writting EEPROM...");
    if (newInverterIp.length() > 0 && inverter::inverterIp != newInverterIp) {
      inverter::inverterIp = newInverterIp;
      Serial.print("Writting inverterIp: ");
      Serial.println(inverter::inverterIp);
      eeprom::setInverterIp(inverter::inverterIp);
      inverter::setInverterIp(inverter::inverterIp);
      controller::heaterTriggering(-1);
    }
    if (newInverterPort.length() > 0 && inverter::inverterPort != newInverterPort.toInt()) {
      inverter::inverterPort = newInverterPort.toInt();
      Serial.print("Writting inverterPort: ");
      Serial.println(inverter::inverterPort);
      eeprom::setInverterPort(inverter::inverterPort);
      controller::heaterTriggering(0);
    }
    if (newPowerLimit.length() > 0 && controller::powerLimit != newPowerLimit.toInt()) {
      controller::powerLimit = newPowerLimit.toInt();
      Serial.print("Writting powerLimit: ");
      Serial.println(controller::powerLimit);
      eeprom::setPowerLimit(controller::powerLimit);
      controller::heaterTriggering(inverter::getPower());
    }
    if (newHysteresis.length() > 0 && controller::hysteresis != newHysteresis.toInt()) {
      controller::hysteresis = newHysteresis.toInt();
      Serial.print("Writting hysteresis: ");
      Serial.println(controller::hysteresis);
      eeprom::setHysteresis(controller::hysteresis);
      controller::heaterTriggering(inverter::getPower());
    }
    if (newWorkMode.length() > 0 && controller::workMode != newWorkMode) {
      controller::workMode = newWorkMode;
      Serial.print("Writting workMode: ");
      Serial.println(controller::workMode);
      eeprom::setWorkMode(controller::workMode);
      controller::heaterTriggering(inverter::getPower());
    }

    eeprom::commit();
    server.send(200, "text/html", settings::getHtml(inverter::inverterIp, String(inverter::inverterPort), String(controller::powerLimit), String(controller::hysteresis), _prepareWorkModes()));
  });
}

String _prepareWorkModes() {
  String result = "<option";
  if (controller::workMode == "ON") {
    result += " selected";
  }
  result += ">ON</option>";

  result += "<option";
  if (controller::workMode == "OFF") {
    result += " selected";
  }
  result += ">OFF</option>";

  result += "<option";
  if (controller::workMode == "AUTO") {
    result += " selected";
  }
  result += ">AUTO</option>";

  return result;
}

}
