namespace wifi_configuration {

void setup();
void setupWifiConfiguration();

void _launchWifiConfig();
bool _testWifi();
void _setupAP();
void _configureEndpoints();
bool _enableLed();
bool _startDelayTimer();
void _configTimers();
void _createWebServer();

}
