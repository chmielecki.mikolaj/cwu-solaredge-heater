#include "eeprom.h"

namespace eeprom {

int ssidLen = 32;
int passLen = 32;
int ipLen = 15;
int gatewayIpLen = 15;
int inverterIpLen = 15;
int hysteresisLen = 3;
int powerLimitLen = 4;
int workModeLen = 4;
int inverterPortLen = 5;

int ssidStart = 0;
int passStart = ssidLen;
int ipStart = passStart + passLen;
int gatewayIpStart = ipStart + ipLen;
int inverterIpStart = gatewayIpStart + gatewayIpLen;
int hysteresisStart = inverterIpStart + inverterIpLen;
int powerLimitStart = hysteresisStart + hysteresisLen;
int workModeStart = powerLimitStart + powerLimitLen;
int inverterPortStart = workModeStart + workModeLen;

int ssidEnd = ssidStart + ssidLen;
int passEnd = passStart + passLen;
int ipEnd = ipStart + ipLen;
int gatewayIpEnd = gatewayIpStart + gatewayIpLen;
int inverterIpEnd = inverterIpStart + inverterIpLen;
int hysteresisEnd = hysteresisStart + hysteresisLen;
int powerLimitEnd = powerLimitStart + powerLimitLen;
int workModeEnd = workModeStart + workModeLen;
int inverterPortEnd = inverterPortStart + inverterPortLen;

String getSsid() {
  String ssid = "";
  for (int i = ssidStart; i < ssidEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    ssid += newChar;
  }
  return ssid;
}

void setSsid(String ssid) {
  for (int i = 0; i < ssidLen; ++i) {
    EEPROM.write(ssidStart + i, 0);
  }
  for (int i = ssidStart; i < ssid.length(); ++i) {
    EEPROM.write(ssidStart + i, ssid[i]);
  }
}


String getPass() {
  String pass = "";
  for (int i = passStart; i < passEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    pass += newChar;
  }
  return pass;
}

void setPass(String pass) {
  for (int i = 0; i < passLen; ++i) {
    EEPROM.write(passStart + i, 0);
  }
  for (int i = 0; i < pass.length(); ++i) {
    EEPROM.write(passStart + i, pass[i]);
  }
}


String getIp() {
  String ip = "";
  for (int i = ipStart; i < ipEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    ip += newChar;
  }
  return ip;
}

void setIp(String ip) {
  for (int i = 0; i < ipLen; ++i) {
    EEPROM.write(ipStart + i, 0);
  }
  for (int i = 0; i < ip.length(); ++i) {
    EEPROM.write(ipStart + i, ip[i]);
  }
}


String getGatewayIp() {
  String gatewayIp = "";
  for (int i = gatewayIpStart; i < gatewayIpEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    gatewayIp += newChar;
  }
  return gatewayIp;
}

void setGatewayIp(String gatewayIP) {
  for (int i = 0; i < gatewayIpLen; ++i) {
    EEPROM.write(gatewayIpStart + i, 0);
  }
  for (int i = 0; i < gatewayIP.length(); ++i) {
    EEPROM.write(gatewayIpStart + i, gatewayIP[i]);
  }
}


String getInverterIp() {
  String inverterIp = "";
  for (int i = inverterIpStart; i < inverterIpEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    inverterIp += newChar;
  }
  return inverterIp;
}

void setInverterIp(String inverterIp) {
  for (int i = 0; i < inverterIpLen; ++i) {
    EEPROM.write(inverterIpStart + i, 0);
  }
  for (int i = 0; i < inverterIp.length(); ++i) {
    EEPROM.write(inverterIpStart + i, inverterIp[i]);
  }
}


int getHysteresis() {
  String hysteresis = "";
  for (int i = hysteresisStart; i < hysteresisEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    hysteresis += newChar;
  }
  return hysteresis.toInt();
}

void setHysteresis(int hysteresis) {
  String _hysteresis = String(hysteresis);
  for (int i = 0; i < hysteresisLen; ++i) {
    EEPROM.write(hysteresisStart + i, 0);
  }
  for (int i = 0; i < _hysteresis.length(); ++i) {
    EEPROM.write(hysteresisStart + i, _hysteresis[i]);
  }
}


int getPowerLimit() {
  String powerLimit = "";
  for (int i = powerLimitStart; i < powerLimitEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    powerLimit += newChar;
  }
  return powerLimit.toInt();
}

void setPowerLimit(int powerLimit) {
  String _limitPower = String(powerLimit);
  for (int i = 0; i < powerLimitLen; ++i) {
    EEPROM.write(powerLimitStart + i, 0);
  }
  for (int i = 0; i < _limitPower.length(); ++i) {
    EEPROM.write(powerLimitStart + i, _limitPower[i]);
  }
}


String getWorkMode() {
  String workMode = "";
  for (int i = workModeStart; i < workModeEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    workMode += newChar;
  }
  return workMode;
}

void setWorkMode(String workMode) {
  for (int i = 0; i < workModeLen; ++i) {
    EEPROM.write(workModeStart + i, 0);
  }
  for (int i = 0; i < workMode.length(); ++i) {
    EEPROM.write(workModeStart + i, workMode[i]);
  }
}


int getInverterPort() {
  String inverterPort = "";
  for (int i = inverterPortStart; i < inverterPortEnd; ++i) {
    char newChar = char(EEPROM.read(i));
    if (!newChar) break;
    inverterPort += newChar;
  }
  return inverterPort.toInt();
}

void setInverterPort(int inverterPort) {
  String _inverterPort = String(inverterPort);
  for (int i = 0; i < inverterPortLen; ++i) {
    EEPROM.write(inverterPortStart + i, 0);
  }
  for (int i = 0; i < _inverterPort.length(); ++i) {
    EEPROM.write(inverterPortStart + i, _inverterPort[i]);
  }
}


void commit() {
  EEPROM.commit();
}


}