#include "Arduino.h"
#include <EEPROM.h>


namespace eeprom {

String getSsid();
void setSsid(String ssid);

String getPass();
void setPass(String pass);

String getIp();
void setIp(String ip);

String getGatewayIp();
void setGatewayIp(String gatewayIp);

String getGatewayIp();
void setGatewayIp(String gatewayIp);

String getInverterIp();
void setInverterIp(String inverterIp);

int getHysteresis();
void setHysteresis(int hysteresis);

int getPowerLimit();
void setPowerLimit(int limitPower);

String getWorkMode();
void setWorkMode(String workMode);

int getInverterPort();
void setInverterPort(int inverterPort);

void commit();

}