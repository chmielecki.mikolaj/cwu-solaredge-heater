
namespace constants {

const int SMALL_DELAY = 10;
const int NORMAL_DELAY = 100;
const int LARGE_DELAY = 500;
const int SERVER_RESPONSE_DELAY = 1000;
const int LED_ON_TIME = 100;
const int LED_OFF_TIME_INVERTER_CONNECTING = 5000;
const int LED_OFF_TIME = 10000;

const int CONTROLLER_OUTPUT = 14;

const int INVERTER_REFRESH_TIME = 3000;
const int INVERTER_POWER_REGISTER = 40079;

const int CHECK_NTP_TIME = 1000 * 60 * 10;
const int MIN_WORKING_TIME = 1000 * 60 * 20;

}