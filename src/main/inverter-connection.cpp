#include "inverter-connection.h"
#include <Pinger.h>
#include <ESP8266WiFi.h>
extern "C" {
#include <lwip/icmp.h>  // needed for icmp packet definitions
}

namespace inverterConnection {
bool isConnected = false;
Pinger pinger;

const int PING_REPLAY_NUMBER = 4;

void setup() {
  pinger.OnReceive([](const PingerResponse& response) {
    return true;
  });

  pinger.OnEnd([](const PingerResponse& response) {
    if (response.TotalReceivedResponses > 0) {
      isConnected = true;
      Serial.println("Connect to inverter, ping successfull: " + String(response.TotalReceivedResponses) + "/" + String(PING_REPLAY_NUMBER));
    } else {
      isConnected = false;
      Serial.println("Cannot ping to inverter");
    }

    return true;
  });
}

void checkConnectionLoop(IPAddress remote) {
  pinger.Ping(remote, PING_REPLAY_NUMBER);
}

}