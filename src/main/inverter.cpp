#include <limits.h>
#include "inverter.h"
#include "constants.h"
#include <arduino-timer.h>
#include "controller.h"
#include "inverter-connection.h"

namespace inverter {

String inverterIp = "";
int inverterPort = 0;
IPAddress remote;
ModbusIP mb;
bool isConnectedToInverter = false;

// {L1 [V], L2 [V], L3 [V], scale factor, power [W], scale factor, frequency [Hz], scale factor}
int16_t registers[8] = { 0, 0, 0, 1, 0, 1, 0, 1 };

auto _refreshTimer = timer_create_default();

void setup() {
  Serial.println("Reading EEPROM...");
  inverterIp = eeprom::getInverterIp();
  Serial.print("Inverter IP: ");
  Serial.println(inverterIp);
  inverterPort = eeprom::getInverterPort();
  Serial.print("Inverter port: ");
  Serial.println(inverterPort);

  remote.fromString(inverterIp);
  mb.client();
  _refreshTimer.every(constants::INVERTER_REFRESH_TIME, _inverterRefreshing);
}

void loop() {
  _refreshTimer.tick();
  mb.task();
}

float getPower() {
  if (!isConnectedToInverter) {
    Serial.println("Not connected to inverter");
    return -1;
  }
  return registers[4] * pow(10, registers[5]);
}

void getVoltage(float* result) {
  if (!isConnectedToInverter) {
    Serial.println("Not connected to inverter");
    result[0] = -1;
    result[1] = -1;
    result[2] = -1;
  }
  const float scale = pow(10, registers[3]);
  result[0] = registers[0] * scale;
  result[1] = registers[1] * scale;
  result[2] = registers[2] * scale;
}

float getFrequency() {
  if (!isConnectedToInverter) {
    Serial.println("Not connected to inverter");
    return -1;
  }
  return registers[6] * pow(10, registers[7]);
}


void setInverterIp(String ip) {
  Serial.println("Disconnecting from inverter and changing IP address");
  mb.disconnect(remote);
  remote.fromString(inverterIp);
}

bool _inverterRefreshing(void*) {
  inverterConnection::checkConnectionLoop(remote);

  if (mb.isConnected(remote) && inverterConnection::isConnected) {
    isConnectedToInverter = true;
    Serial.println("Connected to inverter...");
    mb.readHreg(remote, constants::INVERTER_POWER_REGISTER, (uint16_t*)registers, 8, nullptr, 1);
  } else {
    isConnectedToInverter = false;
    Serial.println("Connecting to inverter...");
    mb.connect(remote, 502);
    registers[0] = 0;
    registers[1] = 0;
    registers[2] = 0;
    registers[3] = 1;
    registers[4] = 0;
    registers[5] = 1;
    registers[6] = 0;
    registers[7] = 1;
  }
  controller::heaterTriggering(getPower());
  return true;
}
}