#!/bin/bash

while getopts ":h" option; do
   case $option in
      h) echo "Kolejność argumentów: <nazwa pliku bez .html>, ...<zmienne>"
         exit;;
   esac
done

if [ -n "$1" ]
then

  STYLES_LN=$(awk '/_STYLES_/{ print NR; exit }' ${1}.html)
  cp ${1}.html tmp.html

  ## usuwanie linii z "$STYLES"
  ## ważne aby za "$STYLES" była pusta linia
  sed "${STYLES_LN}d" ${1}.html > tmp.html

  ## wstrzykiwanie globalnego pliku styles.css w miejsce "$STYLES"
  perl -i -slpe 'print $s if $. == $n; $. = 0 if eof' -- -n=$STYLES_LN -s="$(cat styles.css)" tmp.html

  # parsowanie do jednej linii i usuwanie wielokrotnych spacji
  cat tmp.html | tr -d '\n' | sed -e's/  */ /g' > previews/"${1}".html
  rm tmp.html

  # zastępowanie
  ## " -> \"
  ## +' -> +"
  ## "+ -> "+
  PAGE=$(cat previews/"${1}".html | sed 's/\"/\\\"/g' | sed "s/'+/\" +/g" | sed "s/+'/+ \"/g")

  NAMESPACE=$(echo ${1} | sed 's/-/_/g')

  # przygotowywanie prototypu funkcji
  if (( $# > 1 )); then
      VARIABLES="String "
      VARIABLES+=$2

      variables=("$@")

      for variable in "${variables[@]:2}"; do
        VARIABLES+=", String "
        VARIABLES+=$variable
      done
  fi

  read -r -d '' OUTPUT_H <<- EOM
#include "Arduino.h"
namespace $NAMESPACE {
    String getHtml($VARIABLES) {
        return String("$PAGE");
    }
}
EOM

  echo "$OUTPUT_H" > ../"${1}"-html.h
else
  echo "Podaj nazwe pliku html"
fi
