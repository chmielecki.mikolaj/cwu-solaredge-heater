# cwu-solaredge-heater

## Uruchomienie

Po podłączeniu urządzenia do zasialania, próbuje nawiązać połączenie z siecią Wi-Fi. 
Jeżeli jest to pierwsze uruchomienie urządzenie przechodzi w tryb konfiguracji Wi-Fi.
Wtedy należy skonfigurować urządzenie.

## Konfiguracjia Wi-Fi:
```
"cwu-solaredge" - ssid w trybie konfiguracji Wi-Fi, brak hasła
192.168.4.1 - domyślny adres ip w trybie konfiguracji Wi-Fi
```
Ustawiamy następujące parametry:
- SSID - nazwa domowej sieci Wi-Fi,
- hasło - hasło do domowej sieci Wi-Fi,
- adres IP - adres IP urządzenia, adres musi należeć zbioru adresów domowej sieci,
- adres IP bramy - adres IP ruotera

## Status urządzenia:
- LED mruga po włączeniu - nawiązywanie połączenia z domowym Wi-Fi
- LED świeci światłem ciągłym - tryb konfiguracji Wi-Fi
- LED miga co 5 sekund - połączony z WiFi, brak połączenia z falownikiem
- LED miga co 10 sekund - tryb normalnej pracy

## Normalna praca urządzenia
Podczas normalnej pracy urządzenia, urządzenie udostępnia interfejs konfiguracyjny w przeglądarce internetowej.
W przeglądarce internetowej należy wpisać adres IP urządzenia.
Pojawi się strona informacyjna. Strona informacyjna zawiera odnośnik do strony ustawień "Ustawienia".

Na stronie ustawień można zmieniać parametry pracy urządzenia. Każda zmiana ustawień wymaga potwierdzenia zmian poprzez kliknięcie przycisku "Zapisz".
